<?php

namespace app\controllers;
use yii;
use app\models\Post;
use app\models\PostSearch;
use DateTime;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\web\UploadedFile;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing as WorksheetDrawing;
use yii\rest\ActiveController;


// use PhpOffice\PhpSpreadsheet\Writer\CVS;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet;


// require 'vendor/autoload.php';




/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public static function dateIndo(){
        date_default_timezone_set('Asia/Jakarta');
        $timezone = time() + (60 * 60 * 7);
        $waktu = gmdate('d-m-Y H:i:s', $timezone);
        return $waktu;
    }
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        // echo  "<pre>"; print_r($dataProvider); die;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            
            
        ]);
    }

    /**
     * Displays a single Post model.
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();
        $random = rand();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $name = UploadedFile::getInstance($model, 'gambarBaru');
                $path = 'uploads/'.$random.md5($name->baseName). '.' .$name->extension;

                if($name->saveAs($path)){
                    $model->gambar = $name->baseName . '.' .$name->extension;
                    $model->filepath = $path;
                    //  echo '<pre>'; print_r($model->filepath); die;
                    $model->save();
                    // if($model->save()){
                    //     // echo '<pre>'; print_r($model); die;
                    //     return //$this->redirect(['index']);
                    // }
                }
                return  $this->redirect(['view', 'id' => $model->id]);
            }
        }else {
            $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
        ]);

        // if ($this->request->isPost) {
        //     if ($model->load($this->request->post()) &&$model->save()) {
        //         echo '<pre>'; print_r($this->request->post()); die;

        //         return $this->redirect(['view', 'id' => $model->id]);
        //     }
        // } else {
        //     $model->loadDefaultValues();
        // }

        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $gambar = $model->filepath;
        $gg = $model->gambar;
        $random = rand();
        // $sama = $model = $this->findModel($id);
        // echo '<pre>'; print_r($gambar); die; 
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                    //  echo '<pre>'; print_r($model->gambarBaru); die; 
                // form inputs are valid, do something here
                $name = UploadedFile::getInstance($model, 'gambarBaru');
                // echo '<pre>'; print_r($name); die;
                if(!empty($name)){
                    
                
                $path = 'uploads/'.$random.md5($name->baseName). '.' .$name->extension;
                if($name->saveAs($path)){
                    if($model->gambar){
                        // echo '<pre>'; print_r($model->gambar); die;
                        // echo '<pre>'; print_r($name); die;
                        unlink(getcwd().'/'.$gambar);
                        
                        // echo '<pre>'; print_r($model->gambar->extension); die; 
                        // unlink(Yii::app()->basePath.'/../images/uploaded/'. $model->filepath);
                    }
                    // $model->gambar = $gg;
                    $model->gambar = $name->baseName . '.' .$name->extension;
                    $model->filepath = $path;
                    // $model->filepath = $gambar;
                //    echo '<pre>'; print_r($model); die; 
                    
                }
                
            }
                $model->save();
                return  $this->redirect(['view', 'id' => $model->id]);
                // // $model->save();
                // return  $this->redirect(['view', 'id' => $model->id]);
            }
        }else {
                $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
        ]);
        
        
        
        
        
        
        
        
        
        // if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
        $model = Post::findOne($id);
        $model->title;
       
        // echo '<pre>'; print_r($model); die;

        if(!empty($model->filepath)){
        unlink(getcwd().'/'.$model->filepath);
        }



        // echo '<pre>'; print_r($model->title); die;


        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    // public function deleteImage()

    //     { 

    //         unlink(Yii::app()->basePath.'uploads/'.$this->image);

    //     }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            // echo '<pre>'; print_r($model); die; 

            return $model;
            
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    // public function actionUpload(){
    //     $model = new Post();

    //     if ($model->load(Yii::$app->request->post())) {
    //         if ($model->validate()) {
    //             // form inputs are valid, do something here
    //             $name = UploadedFile::getInstance($model, 'gambar');
    //             $path = 'uploads/'.md5($name->baseName). '.' .$name->extension;
    //             if($name->saveAs($path)){
    //                 $model->gambar = $name->baseName . '.' .$name->extension;
    //                 $model->filepath = $path;
    //             }
    //             return;
    //         }
    //     }

    //     return $this->render('post', [
    //         'model' => $model,
    //     ]);
    // }

    public function actionExport()
    {
            // $model = new Post() Post::find()->all();;
            // $data = Post::find()->where(['gambar'=> 'preview.png'])
            // ->all();
            $data = Post::find()->all();

            // return 'i'-> $data =>i;
            $pp = $data;
            // echo '<pre>'; print_r($data); die;
            // form inputs are valid, do something here
        //    echo '<pre>'; print_r($pp); die;
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->mergeCells('A2:D2');
            // $sheet->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A2:B2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A2:D2')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('00ff00');
            // $sheet->getColumnDimension('A')->setWidth(120, 'pt');
            $sheet->getColumnDimension('A')->setWidth(15);
            $sheet->getColumnDimension('B')->setWidth(30);
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('D')->setWidth(40);

            $sheet->setCellValue('A2', 'DAFTAR POSTINGAN');
            $sheet->getStyle('A2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK);
            
            $sheet->getStyle('A2:D3')
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('A3', 'Title');
            $sheet->setCellValue('B3', 'description');
            $sheet->setCellValue('C3', 'tanggal');
            $sheet->setCellValue('D3', 'Gambar');
            
            // $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            // $drawing->setName('Paid');
            // $drawing->setDescription('Paid');
            // $drawing->setPath('uploads/preview.png'); // put your path and image here
            // $drawing->setCoordinates('D5');
            // $drawing->setWidth(120);

            // $drawing->setHeight(150);
            // $drawing->setWorksheet($sheet);

            $dd= "uploads/ffd4b9ddd66763228cf84d0d4fc4b04d.webp";
            // $waktu = time();
            
            $waktu = self::dateIndo();
            // echo '<pre>'; print_r($zz); die;
            

            $rowCount = 4;
            foreach($pp as $ex )
            {
                $sheet->getStyle('A'.$rowCount.':C'.$rowCount)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $drawing = new WorksheetDrawing();
                // echo '<pre>'; print_r($pp); die;
                $sheet->getStyle('C'.$rowCount)
                ->getNumberFormat()
                ->setFormatCode(
                \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME
                );

                $sheet->getStyle('A'.$rowCount.':D'.$rowCount)
                ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                // $value = $sheet->getCell('C'.$rowCount, $ex->tanggal)->getValue();
                // $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value);
                
                // echo '<pre>'; print_r($ex); die;
            $sheet->setCellValue('A'.$rowCount, $ex->title);
            $sheet->setCellValue('B'.$rowCount, $ex->description);
            $sheet->setCellValue('C'.$rowCount, $ex->tanggal);

            
            
            // $sheet->setCellValue('D'.$rowCount, $drawing);
            // echo '<pre>'; print_r($ex->gambar); die;
            // $sheet->setCellValue('A'.$rowCount,  $Pexport['description']);
            $sheet->getRowDimension($rowCount)->setRowHeight(120);
            if($ex->filepath){
                if(is_file($ex->filepath))
                {
                $drawing->setPath($ex->filepath); // put your path and image here
                $drawing->setCoordinates('D'.$rowCount);
                $drawing->setWidth(100);
                $drawing->setOffsetX(5);
                $drawing->setOffsetY(5);
                $drawing->setHeight(150);
                $drawing->setWorksheet($sheet);
                // $sheet->getHeaderFooter()->addImage($drawing, \PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooter::IMAGE_HEADER_CENTER);
                }
            }   
            $rowCount++;
            }

            
            // echo '<pre>'; print_r($sheet); die;
            
            
            // $drawing->setPath('MMMM.png'); // put your path and image here
            // $drawing->setCoordinates('D6');
            // $drawing->setWidth(120);

            // $drawing->setHeight(150);
            // // echo '<pre>'; print_r($sheet); die;
            // $drawing->setWorksheet($sheet);

            // $writer = new Xlsx($spreadsheet);
            //  $writer = IOFactory::createWrite($spreadsheet, 'xlsx');
            // $writer->save('hell.xlsx');
            // $filename = "data.csv";

            
            
            // $writer->setReadDataOnly(true);
            // $writer->load("05featuredemo.xlsx");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            // header('Content-Disposition: attachment; filename"'.urlencode($filename).'"');
            // $spreadsheet = $writer->load($filename);

            // header('Content-Type: application/vnd.ms-excel');   
            header('Content-Disposition: attachment; filename="'.$waktu.'_Postingan.xlsx"'); 
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit;


            // return $this->render('export', [
            //     'model' => $model,
            // ]);
            //return;
        
    }

    public function actionPrint($id)
    {
        $model = Post::findOne($id);
        $model->title;
        // $ww = $this->$waktu;
        $waktu = self::dateIndo();
        // echo '<pre>'; print_r($waktu); die;
        $mpdf = new \Mpdf\Mpdf();
        // $gambar = $mpdf->Image('MMMM.png', 0, 0, 210, 297, '', true, false);
        $html='<img src="'.$model->filepath.'"/>';

       
        

        $mpdf->WriteHTML(    '<h3 style =" text-align: center;">judul:</h3>
                            <h3 style =" text-align: center;">'.$model->title.'</h3>'.
                            '<br>'
                            .'<h3 style =" text-align: center;">'.$model->description.'</h3>'.
                            '<div style="margin: auto;
                            width: 80%;"><img src="'.$model->filepath.'" width="100%" height="100%"></div>'.
                            '<br>'
                            .'<h3 style =" text-align: center;">tanggal:</h3>
                            <h3 style =" text-align: center;">'.$model->tanggal.'</h3>'.
                            '<br>'
                            .'<h3 style =" text-align: center;">'.$model->created_at.'</h3>'
                        );
        $mpdf->Output('data '.$model->title.'.pdf', 'I');
        

        // echo '<pre>'; print_r($model->title); die;
        


        // $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
}
