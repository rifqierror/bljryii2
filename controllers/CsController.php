<?php

namespace app\Controllers;

use app\models\Cs;
use app\models\CsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsController implements the CRUD actions for Cs model.
 */
class CsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Cs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cs model.
     * @param int $id_cs Id Cs
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_cs)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_cs),
        ]);
    }

    /**
     * Creates a new Cs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cs();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_cs' => $model->id_cs]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_cs Id Cs
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_cs)
    {
        $model = $this->findModel($id_cs);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_cs' => $model->id_cs]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_cs Id Cs
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_cs)
    {
        $this->findModel($id_cs)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_cs Id Cs
     * @return Cs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_cs)
    {
        if (($model = Cs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
