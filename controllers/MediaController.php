<?php

namespace app\controllers;
use yii;
use app\models\Media;
use app\models\NewUser;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\Artikel;



class MediaController extends \yii\web\Controller
{
    public function behaviors()
    {
       
        // $siapa = Yii::$app->user->identity->jabatan;
        return [
            'access' => [
                'class' => AccessControl::class,
        //     //     'rulesConfig' => ['class'=> AccessRule::class
        //     //     'class'=>
        //     // ],
                'only' => ['upload'],
                'rules' => [
                    [
                        'allow' => true,
                        // 'expression'=>'$siapa =="1"',
                        // 'actions' => ["upload"],
                        'roles' => ['@'],
                        'actions' => ["index","upload"],
                        // 'roles' => [$siapa !=='1'],
                        // 'expression'=>"$siapa !==1",
                        // 'matchCallback' => function ($rule, $action) {
                        // }
                       
                    ],
                    // [
                    //     'allow' => true,
                    //     'actions' => ['index','upload'],
                    //     'roles' => ['?'],
                    //     // 'expression'=>"$siapa ==1",
                    // ],
                // 'expression'=>'$siapa =="1"',
                // 'expression'=>'isset($app->user->identity->jabatan)',
                
                ],
            ],
        ];
    //     $siapa = Yii::$app->user->identity->jabatan;
    //     // echo '<pre>'; print_r($model->id); die;
    //     return array(
            
    //         array('allow',
    
    //                 'actions'=>array('index'),
    //                 'only' => ['index'],
    //                 'expression'=>"$siapa==1",
    
    //         ),
    
    //         array('allow',
    
    //                 'actions'=>array('index'),
    //                 'only' => ['index'],
    //                 'expression'=>"$siapa !==1",
    
    //         ),
    
    //     //     array('allow', 
    
    //     //             'actions'=>array('delete','admin'),
    
    //     //             'expression'=>"$siapa ==3",
    
    //     //    )
    
    // );
        


    }

    public function actionIndex()
    {
        // $emboh    = Yii::$app->user->id;

        // // $siapa = Yii::$app->user->identity->jabatan;
        // // $auth = Yii::$app->authManager;
        
        $data = Media::find()->all();
        
        return $this->render('index' ,['medias'=>$data]);

        //     array('allow',
    
        //             'actions'=>array('index','upload'),
    
        //             'expression'=>"$siapa == 1 ",
    
        // );
    }

    public function actionUpload(){
        /// $siapa = Yii::$app->user->identity->jabatan;
        $model = new media(); 
        // echo '<pre>'; print_r($siapa); die;
        // if($siapa !=='1'){
        //     Yii::$app->session->setFlash('error', "hanya admin yg bisa upload"); 
        // return $this->redirect(['index']);
        // // die;
        // }
        
        if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
                        
            // form inputs are valid, do something here
            $name = UploadedFile::getInstance($model, 'filename');
            $path = 'uploads/'.md5($name->baseName). '.' .$name->extension;
            if($name->saveAs($path)){
                $model->filename = $name->baseName . '.' .$name->extension;
                $model->filepath = $path;
                if($model->save()){
                return $this->redirect(['index']);
                }
            }
            return;
                        
        }
        }

            return $this->render('upload', [
                'model' => $model,
            ]);
                
    }
    

}
