<?php

namespace app\controllers;
use yii;
use app\models\Media;
use app\models\NewUser;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\AccessRule;


class RuleUserController extends \yii\web\Controller
{
    public function behaviors()
    {
        $siapa = Yii::$app->user->identity->jabatan;
        return [
            'access' => [
                'class' => AccessControl::class,
        //     //     'rulesConfig' => ['class'=> AccessRule::class
        //     //     'class'=>
        //     // ],
                'only' => ['index','upload'],
                'rules' => [
                    [
                        'allow' => true,
                        // 'expression'=>'$siapa =="1"',
                        'actions' => ["upload"],
                        'roles' => [$siapa !=='1'],
                        'expression'=>"$siapa !==1",
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','upload'],
                        'roles' => [$siapa =='1'],
                        'expression'=>"$siapa ==1",
                    ],
                // 'expression'=>'$siapa =="1"',
                // 'expression'=>'isset($app->user->identity->jabatan)',
                
                ],
            ],
        ];
    }

}