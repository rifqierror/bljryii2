<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Post;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use kartik\icons\Icon;
// use yii\bootstrap4\Html;
use rmrevin\yii\fontawesome\FA;
use rmrevin\yii\fontawesome\FontAwesome;




 
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">


<!-- <?// echo  "<pre>"; print_r($dataProvider); die; ?>  -->

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo Html::a('Export', ['export'], ['class'=>'btn btn-primary']); ?>

    <!-- data -->




    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'tanggal',
                'tanggal',
                'kategori.nama_kategori',
            // [
            //     'attribute'=>'Tanggal',
            //     'value'=>function($model){
            //         return date("d-m-y", strtotime($model->tanggal)); 
            //     }  
                
            //     // 'format' => ['image',['width'=>'100','height'=>'100']],
            //  ],

            [
                'attribute'=>'gambar',
                'value'=>function($model){
                    return('@web/'.$model->filepath); 
                },  
                
                'format' => ['image',['width'=>'100','height'=>'100']],
             ],

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' =>'menu',
                'template'=>'{leadUpdate}{leadDelete}{leadPrint}',

    
                
                'buttons' =>[
                    'leadUpdate'=>function($url, $model){
                        $url = Url::to(['update','id'=>$model->id]);
                        // echo '<pre>'; print_r($url); die;
                        return Html::a('<i class ="fa fa-camera-retro"> update </i>',$url,['class'=>'btn btn-warning',
                                                                                            'style'=>'color:#ffffff;
                                                                                                      font-size: 10px;font-weight:300;',
                    ]);
                    },
                   
                    'leadDelete'=>function($url, $model){
                        $url = Url::to(['delete','id'=>$model->id]);
                        echo '<br>';
                        // echo '<pre>'; print_r($); die;
                        return Html::a('<span class ="fas fa-trash"> Delete</span>',$url,[
                            // 'title'=>'hapus',
                            'data-method'=>'post',
                            'class'=>'btn btn-danger',
                            'style'=>'
                                      color:#fff;
                                      font-size: 10px;
                                      font-weight:300;
                                      font-family:sans-serif',]);
                        
                    },
                    'leadPrint'=>function($url, $model){
                        $url = Url::to(['print','id'=>$model->id]);

                        // echo '<pre>'; print_r($url); die;
                        return Html::a('<span><i class="fas fa-trash"> Edit</i></span>',$url,[
                            // 'title'=>'hapus',
                            // 'data-method'=>'get',
                            'class'=>'btn btn-primary',
                            'style'=>'color:#ffffff;
                                      font-size: 10px;
                                      font-weight:300;',]);
                        
                    },
                ]
                
            ]
        ],
    ]); ?>

         <span><i class="fa fa-camera-retro fa-lg" ></i></span> Home

         <a class="list-group-item"><span><i class="fa fa-camera-retro fa-lg" ></i></span><i class="fa fa-camera-retro fa-lg" ></i></span>xx</a>
         <a href=""><span><i class="fa fa-camera-retro fa-lg" >khj</i></span></a>

         

    </div>


</div>
