<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\bootstrap4\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tanggal')->widget(DateTimePicker::classname(), [
    'options' => ['placeholder' => 'Enter event time ...'],
    'pluginOptions' => [
          'autoclose' => true
    ]
]); ?>

    <?= $form->field($model, 'gambarBaru')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <div>
      <label">Status
          <p>
              ON
             <input type="radio" class="flat" name="gender" id="statusOn" checked="" />
             Off
             <input type="radio" class="flat" name="gender" id="statusOff" checked="" />
          </p>
          
      </label>  
    </div>
    

    <?php ActiveForm::end(); ?>

</div>
