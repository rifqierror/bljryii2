<?php
/* @var $this yii\web\View */

use PHPUnit\Util\Log\JSON;
use yii\web\JsExpression;
use richardfan\widget\JSRegister;
use yii\bootstrap4\Breadcrumbs;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap4\Modal;
?>
<div class="container">
<h1>film/index</h1>
</div>
<?php
        // Modal::begin([
        //     'title' => 'Hello world',
        //     'toggleButton' => ['label' => 'click me','class' =>'btn btn-dark'],
        // ]);

        // echo 'Say hello...';

        // Modal::end();
?>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="input-group ">
                <input type="text" class="form-control" placeholder="judul film" aria-label="Recipient's username" aria-describedby="button-addon2" id="judul">
                <div class="input-group-append">
                    <button class="btn btn-dark" type="button" id="btnfilm">Button</button>
                    <!-- <button class="btn btn-dark" data-toggle="modal" data-target="#exampleModal">aaa</button> -->
                </div>
            </div>
        </div>
    </div>

</div>

<div class="container">
    <div class="row" id="movie-list" style="visibility:hidden">
        <div class="col-md-12">
            <br>
        <div class="alert alert-danger" role="alert">
             Judul film tidak ditemukan
        </div>
        </div>
    
    </div>
</div>


<div class="container">
    <div class="row" id="isi-film">
        <!-- <h1>nnnn</h1> -->
        
    
    </div>
</div>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php

// $this->registerJsFile(
//     '@web/js/main.js',
//     ['depends' => [\yii\web\JqueryAsset::class]]
// );
?>
<?php JSRegister::begin([
    'key' => 'bootstrap-modal',
    'position' => \yii\web\View::POS_READY
    ]); 
?>
<script>
    function cariFilm(){
        $('#isi-film').html('');
        // $.getJSON('http://omdbapi.com?apikey=')
        $.ajax({
            url:'http://omdbapi.com',
            type:'get',
            dataType: 'json',
            data:{
                'apikey' : 'a641ce24',
                's': $('#judul').val()
            },
            success: function (result){
                
                if(result.Response == 'True'){
                    
                    let movies = result.Search
                    
                    //  console.log(movies);
                    $.each(movies, function(i,data){
                        $('#isi-film').append('<div class="col-md-4"><div class="card"><img class="card-img-top" src="'+data.Poster+'" alt="Card image cap"><div class="card-body"><h5 class="card-title">'+data.Title+'</h5><h6 class="card-subtitle mb-2 text-muted">'+data.Year+'</h6><a href="#" class="card-link see-detail" data-toggle="modal" data-target="#exampleModal" data-id="'+data.imdbID+'">Lihat detail</a></div></div></div>');


                        // console.log(data)
                        
                    })
                     
                    

                    $('#judul').val('');
                    $('#movie-list').css('visibility', 'hidden');
                }else{
                $('#movie-list').css('visibility', 'visible');
                };

            }
        })
    }




    $('#btnfilm').on('click',function(){
        cariFilm();

    })

    $('#judul').on('KeyUp', function(e){
        if(e.which === 13 ){
            cariFilm();
        }
    })

    $('#isi-film').on('click', '.see-detail',function(){
        // console.log($(this).data('id'));
        $.ajax({
            url:'http://omdbapi.com',
            datatype: 'json',
            type :'get',
            data:{
                'apikey' : 'a641ce24',
                'i' : $(this).data('id')
            },
            success: function(movie){
                if(movie.Response === "True"){
                    // console.log('ttt');
                    
                    // $('.modal-body').html(<p>asdsadsa </p>);
                     $('.modal-body').html('<div class="container-fluid"><div class="row"><div class="col-md-4"><img src="'+movie.Poster+'" class="img-fluid"></div><div class="col-md-8"><ul class="list-group"><li class="list-group-item"><h3>'+movie.Title+'</h3></li></div></div></div>');
                }
                
            }

        })

    });
</script>
<?php JSRegister::end(); ?>

<!-- $this->registerJs("

    $('#btnfilm').on('click',function(){
        $.getJSON('http://omdbapi.com?apikey=')
        $.ajax({
            url:'http://omdbapi.com',
            type:'get',
            dataType: 'json',
            data:{
                'apikey' : 'a641ce24',
                's': $('#judul').val()
            },
            success: function (result){
                
                if(result.Response == 'True'){
                    
                    let movies = result.Search
                    
                     console.log(movies);
                     
                    


                }else{
                $('#movie-list').css('visibility', 'visible');
                };

            }
        })

    })
");

?> -->


<!-- <script>

// console.log("dsfsd");

//         $('#btnfilm').on('click',function(){    
            
//                 console.log('asas');

//         });
    $('#btnfilm').on('click',function(){
            $.getJSON('http://omdbapi.com?apikey=')
            $.ajax({
                url:'http://omdbapi.com',
                type:'get',
                dataType: 'json',
                data:{
                    'apikey' : 'a641ce24',
                    's': $('#judul').val()
                },
                success: function (result){
                    console.log(result);
                }
            });

        });
</script> -->

