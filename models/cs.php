<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cs".
 *
 * @property int $id_cs
 * @property string $nama
 * @property string $password_cs
 */
class cs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'password_cs'], 'required'],
            [['nama', 'password_cs'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cs' => 'Id Cs',
            'nama' => 'Nama',
            'password_cs' => 'Password Cs',
        ];
    }
}
