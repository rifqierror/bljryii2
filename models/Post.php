<?php

namespace app\models;

use Yii;
use app\models\Kategori;


/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string $tanggal
 *
 * @property Kategori[] $kategoris
 */
class Post extends \yii\db\ActiveRecord
{
    // public function fields()
    // {
    //     return ['id','title','description'];
    // }

    
    public $gambarBaru;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */


    public function rules()
    {
        return [
            [['title', 'description', 'tanggal','id_kategori'], 'required'],
            [['description','gambar','filepath'], 'string'],
            [['created_at', 'updated_at', 'tanggal','gambarBaru'], 'safe'],
            [['title'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Title',
            'description' => 'Description',
            'tanggal' => 'Tanggal',
            'gambar' => 'Gambar',
        ];
    }

    /**
     * Gets query for [[Kategoris]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getKategoris()
    // {
    //     return $this->hasOne(Kategori::className(), ['id' => 'id']);
    // }
    
    public function getKategori()
    {
        return $this->hasOne(Kategori::className(), ['id_kategori' => 'id_kategori']);
    }

    // public function getSubcategory()
    // { 
    // return $this->hasMany(Kategori::className(), ['id' => 'id_kategori']); 
    // }
}
