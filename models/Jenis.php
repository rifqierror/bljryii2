<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis".
 *
 * @property int $id_jenis
 * @property string $jenis
 *
 * @property Hewan[] $hewans
 */
class Jenis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis'], 'required'],
            [['jenis'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jenis' => 'Id Jenis',
            'jenis' => 'Jenis',
        ];
    }

    /**
     * Gets query for [[Hewans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHewans()
    {
        return $this->hasMany(Hewan::className(), ['id_jenis' => 'id_jenis']);
    }
}
