<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hewan".
 *
 * @property int $id_hewan
 * @property string $hewan
 * @property int $id_jenis
 *
 * @property Jenis $jenis
 */
class Hewan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hewan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hewan', 'id_jenis'], 'required'],
            [['id_jenis'], 'integer'],
            [['hewan'], 'string', 'max' => 120],
            [['id_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => Jenis::className(), 'targetAttribute' => ['id_jenis' => 'id_jenis']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_hewan' => 'Id Hewan',
            'hewan' => 'Hewan',
            'id_jenis' => 'Id Jenis',
        ];
    }

    /**
     * Gets query for [[Jenis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenis()
    {
        return $this->hasMany(Jenis::className(), ['id_jenis' => 'id_jenis']);
    }
}
